#!/usr/bin/env python3

import os
import sys

ENV_DIR = '/home/ricardo/.virtualenvs/repositorio'
ENV_PKG = '/home/ricardo/.virtualenvs/repositorio/lib/python3.5/site-packages'
APP_DIR = '/home/ricardo/www/repositorio'

sys.path.append(ENV_DIR)
sys.path.append(ENV_PKG)
sys.path.insert(0, APP_DIR)

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "repositorio.settings")

application = get_wsgi_application()
