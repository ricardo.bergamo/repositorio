from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from import_export.admin import ImportExportActionModelAdmin
from .models import Antibiotico, AntibioticoGrupo, AntibioticoSubGrupo
from .models import MicrorganismoGrupo, MicrorganismoClasse, Microrganismo
from .models import CepaRM, CepaTSA, Gene, Material,Metodo, Teste, Vinculo, Estabelecimento
from .models import NormaAcreditacao, NormaQualidade, NormaTSA


def make_active(self, request, queryset):
    rows_updated = queryset.update(status='1')
    if rows_updated == 1:
        message_bit = "1 registro foi"
    else:
        message_bit = "%s registros foram" % rows_updated
    self.message_user(request, "%s ativado(s)" % message_bit)


make_active.short_description = "Ativar registro(s) selecionado(s)"


def make_inactive(self, request, queryset):
    rows_updated = queryset.update(status='2')
    if rows_updated == 1:
        message_bit = "1 registro foi"
    else:
        message_bit = "%s registros foram" % rows_updated
    self.message_user(request, "%s inativado(s)" % message_bit)


make_inactive.short_description = "Inativar registro(s) selecionado(s)"


@admin.register(AntibioticoGrupo)
class AntibioticoGrupoAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin):
    radio_fields = {'status': admin.HORIZONTAL}
    list_display = ['id', 'grupo', 'status']
    list_display_links = ('grupo',)
    list_filter = ['status']
    search_fields = ['grupo']
    actions = [make_active, make_inactive]


@admin.register(AntibioticoSubGrupo)
class AntibioticoSubGrupoAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin):
    radio_fields = {'status': admin.HORIZONTAL}
    list_display = ['id', 'subgrupo', 'status']
    list_display_links = ('subgrupo',)
    list_filter = ['status']
    search_fields = ['subgrupo']
    actions = [make_active, make_inactive]


class AntibioticoResource(resources.ModelResource):
    class Meta:
        model = Antibiotico
        fields = ('id', 'antibiotico', 'grupo__grupo', 'subgrupo__subgrupo', 'status')
        export_order = ('id', 'antibiotico', 'grupo__grupo', 'subgrupo__subgrupo', 'status')


@admin.register(Antibiotico)
class AntibioticoAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin):
    resource_class = AntibioticoResource
    radio_fields = {'status': admin.HORIZONTAL}
    list_display = ['id', 'antibiotico', 'grupo', 'subgrupo', 'status']
    list_display_links = ('antibiotico',)
    list_filter = ['status', 'grupo']
    search_fields = ['antibiotico']
    actions = [make_active, make_inactive]


@admin.register(MicrorganismoGrupo)
class MicrorganismoGrupoAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin):
    radio_fields = {'status': admin.HORIZONTAL}
    list_display = ['id', 'grupo', 'status']
    list_display_links = ('grupo',)
    list_filter = ['status']
    search_fields = ['grupo']
    actions = [make_active, make_inactive]


@admin.register(MicrorganismoClasse)
class MicrorganismoClasseAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin):
    radio_fields = {'status': admin.HORIZONTAL}
    list_display = ['id', 'classe', 'status']
    list_display_links = ('classe',)
    list_filter = ['status']
    search_fields = ['classe']
    actions = [make_active, make_inactive]


class MicrorganismoResource(resources.ModelResource):
    class Meta:
        model = Microrganismo
        fields = ('id', 'microrganismo', 'grupo__grupo', 'classe__classe', 'status')
        export_order = ('id', 'microrganismo', 'grupo__grupo', 'classe__classe', 'status')


@admin.register(Microrganismo)
class MicrorganismoAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin):
    resource_class = MicrorganismoResource
    radio_fields = {'status': admin.HORIZONTAL}
    list_display = ['id', 'microrganismo', 'grupo', 'classe', 'status']
    list_display_links = ('microrganismo',)
    list_filter = ['grupo', 'classe', 'status']
    search_fields = ['microrganismo']
    actions = [make_active, make_inactive]


@admin.register(CepaRM)
class CepaRMAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin):
    radio_fields = {'status': admin.HORIZONTAL}
    list_display = ['id', 'cepa', 'status']
    list_display_links = ('cepa',)
    list_filter = ['status', ]
    search_fields = ['cepa']
    actions = [make_active, make_inactive]


@admin.register(CepaTSA)
class CepaTSAAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin):
    radio_fields = {'status': admin.HORIZONTAL}
    list_display = ['id', 'cepa', 'status']
    list_display_links = ('cepa',)
    list_filter = ['status', ]
    search_fields = ['cepa']
    actions = [make_active, make_inactive]


@admin.register(Gene)
class GeneAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin):
    radio_fields = {'status': admin.HORIZONTAL}
    list_display = ['id', 'gene', 'status']
    list_display_links = ('gene',)
    list_filter = ['status']
    search_fields = ['gene']
    actions = [make_active, make_inactive]


@admin.register(Material)
class MaterialAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin):
    radio_fields = {'status': admin.HORIZONTAL}
    list_display = ['id', 'material', 'status']
    list_display_links = ('material',)
    list_filter = ['status']
    search_fields = ['metodo']
    actions = [make_active, make_inactive]


@admin.register(Metodo)
class MetodoAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin):
    radio_fields = {'status': admin.HORIZONTAL}
    list_display = ['id', 'metodo', 'status']
    list_display_links = ('metodo',)
    list_filter = ['status']
    search_fields = ['metodo']
    actions = [make_active, make_inactive]


@admin.register(NormaAcreditacao)
class NormaAcreditacaoAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin):
    radio_fields = {'status': admin.HORIZONTAL}
    list_display = ['id', 'norma', 'status']
    list_display_links = ('norma',)
    list_filter = ['status',]
    search_fields = ['norma']
    actions = [make_active, make_inactive]


@admin.register(NormaQualidade)
class NormaQualidadeAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin):
    radio_fields = {'status': admin.HORIZONTAL}
    list_display = ['id', 'norma', 'status']
    list_display_links = ('norma',)
    list_filter = ['status',]
    search_fields = ['norma']
    actions = [make_active, make_inactive]


@admin.register(NormaTSA)
class NormaTSAAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin):
    radio_fields = {'status': admin.HORIZONTAL}
    list_display = ['id', 'norma', 'status']
    list_display_links = ('norma',)
    list_filter = ['status',]
    search_fields = ['norma']
    actions = [make_active, make_inactive]


@admin.register(Teste)
class TesteAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin):
    radio_fields = {'status': admin.HORIZONTAL}
    list_display = ['id', 'teste', 'status']
    list_display_links = ('teste',)
    list_filter = ['status']
    search_fields = ['teste']
    actions = [make_active, make_inactive]


@admin.register(Vinculo)
class VinculoAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin):
    radio_fields = {'status': admin.HORIZONTAL}
    list_display = ['id', 'vinculo', 'status']
    list_display_links = ('vinculo',)
    list_filter = ['status']
    search_fields = ['vinculo']
    actions = [make_active, make_inactive]


@admin.register(Estabelecimento)
class EstabelecimentoAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin):
    fieldsets = [
        ('Estabelecimento',
         {'description': '',
          'fields': [
              'registro', 'registro_id', 'cnpj', 'razao_social', 'municipio', 'endereco', 'telefone',
              'email', 'natureza', 'esfera', 'vinculo', 'servico', 'lab_micro', 'lab_apoio',
          ]}),
        ('Inf. Hospitalares',
         {'description': '',
          'fields': [
              'leitos_total', 'leitos_uti', 'leitos_uti_neo', 'internacoes', 'atendimento',
              'nucleo_epidemio', 'lic_sanitaria', 'sciras', 'sinan', 'corpo_clinico', 'acreditacao',
          ]}),
        ('Inf. Laboratoriais',
         {'description': '',
          'fields': [
              'gestao_qualidade', 'resp_tecnico', 'prof_micro', 'interpretacao_tsa', 'metodo_tsa',
              'cq_tsa', 'cepa_tsa', 'teste_rm', 'cq_rm', 'cepa_rm', 'gene_rm', 'cultura', 'hemocultura',
              'urocultura', 'neisseria',
          ]}),
        ('Registro BR-GLASS',
         {'description': '', 'fields': [
             'classificacao', 'data_cadastro', 'data_atualizacao', 'status',
         ]})
    ]
    radio_fields = {
        'natureza': admin.HORIZONTAL, 'esfera': admin.HORIZONTAL, 'servico': admin.HORIZONTAL,
        'lab_micro': admin.HORIZONTAL, 'atendimento': admin.HORIZONTAL, 'nucleo_epidemio': admin.HORIZONTAL,
        'lic_sanitaria': admin.HORIZONTAL, 'sciras': admin.HORIZONTAL, 'sinan': admin.HORIZONTAL,
        'corpo_clinico': admin.HORIZONTAL, 'prof_micro': admin.HORIZONTAL, 'status': admin.HORIZONTAL,
    }
    list_display = ['id', 'razao_social', 'status']
    list_display_links = ('razao_social',)
    list_filter = ['status']
    search_fields = ['razao_social']
    actions = [make_active, make_inactive]

