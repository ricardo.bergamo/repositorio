from django.db import models

ATENDIMENTOS = (
    ('HOS', 'Hospitalar'),
    ('AMB', 'Ambulatorial'),
)

CLINICOS = (
    ('EPI', 'Epidemiologista'),
    ('INF', 'Infectologista'),
)

CLASSIF = (
    ('US', 'Unidade Sentinela'),
    ('LA', 'Laboratório Apoio'),
)

ESFERAS = (
    ('MUN', 'Municipal'),
    ('EST', 'Estadual'),
    ('NAC', 'Nacional'),
)

NATUREZAS = (
    ('PB', 'Pública'),
    ('PV', 'Privada'),
)

NORMAS = (
    ('ACR', 'Acreditação'),
    ('INT', 'Interpretação'),
    ('QLD', 'Qualidade'),
)

NOT_SINAN = (
    ('0', 'Não Notifica'),
    ('1', '10 ou menos'),
    ('2', 'De 11 a 20'),
    ('3', 'De 21 a 50'),
    ('4', 'Acima de 50'),
)

PERIODOS = (
    ('0', 'Não Realiza'),
    ('1', 'Diariamente'),
    ('2', 'Semanalmente'),
    ('3', 'Mensalmente'),
    ('4', 'Anualmente'),
)

SERVICOS = (
    ('PAR', 'Particular'),
    ('SUS', 'Sistema Único de Saúde'),
    ('AMB', 'Ambos'),
)

SIM_NAO = (
    ('SIM', 'Sim'),
    ('NAO', 'Não'),
)

SISTEMAS = (
    ('CNES', 'CNES'),
    ('RSA', 'ANVISA'),
)

STATUS = (
    (1, 'Ativo'),
    (2, 'Inativo'),
)


class AntibioticoGrupo(models.Model):
    __tablename__ = 'glass_antibiotico_grupo'

    id = models.AutoField(primary_key=True, verbose_name='Código')
    grupo = models.CharField(max_length=30, verbose_name='Grupo')
    status = models.SmallIntegerField(choices=STATUS, default=1, verbose_name='Status')

    class Meta:
        ordering = ["grupo"]
        verbose_name = "Antibiótico - Grupo"
        verbose_name_plural = "Antibióticos :: Grupo"

    def __str__(self):
        return self.grupo


class AntibioticoSubGrupo(models.Model):
    __tablename__ = 'glass_antibiotico_subgrupo'

    id = models.AutoField(primary_key=True, verbose_name='Código')
    subgrupo = models.CharField(max_length=30, verbose_name='Sub-Grupo')
    status = models.SmallIntegerField(choices=STATUS, default=1, verbose_name='Status')

    class Meta:
        ordering = ["subgrupo"]
        verbose_name = "Antibiótico - Sub-Grupo"
        verbose_name_plural = "Antibióticos :: Sub-Grupo"

    def __str__(self):
        return self.subgrupo


class Antibiotico(models.Model):
    __tablename__ = 'glass_antibiotico'

    id = models.AutoField(primary_key=True, verbose_name='Código')
    antibiotico = models.CharField(max_length=120, verbose_name='Antibiótico')
    grupo = models.ForeignKey(AntibioticoGrupo, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Grupo')
    subgrupo = models.ForeignKey(AntibioticoSubGrupo, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Sub-Grupo')
    status = models.SmallIntegerField(choices=STATUS, default=1, verbose_name='Status')

    class Meta:
        ordering = ["antibiotico"]
        verbose_name = "Antibiótico"
        verbose_name_plural = "Antibióticos"

    def __str__(self):
        return self.antibiotico


class MicrorganismoGrupo(models.Model):
    __tablename__ = 'glass_microrganismo_grupo'

    id = models.AutoField(primary_key=True, verbose_name='Código')
    grupo = models.CharField(max_length=30)
    status = models.SmallIntegerField(choices=STATUS, default=1, verbose_name='Status')

    class Meta:
        ordering = ["grupo"]
        verbose_name = "Microrganismo :: Grupo"
        verbose_name_plural = "Microrganismos :: Grupo"

    def __str__(self):
        return self.grupo


class MicrorganismoClasse(models.Model):
    __tablename__ = 'glass_microrganismo_classe'

    id = models.AutoField(primary_key=True, verbose_name='Código')
    classe = models.CharField(max_length=30)
    status = models.SmallIntegerField(choices=STATUS, default=1, verbose_name='Status')

    class Meta:
        ordering = ["classe"]
        verbose_name = "Microrganismo :: Classe"
        verbose_name_plural = "Microrganismos :: Classe"

    def __str__(self):
        return self.classe


class Microrganismo(models.Model):
    __tablename__ = 'glass_microrganismo'

    id = models.AutoField(primary_key=True, verbose_name='Código')
    microrganismo = models.CharField(max_length=120)
    grupo = models.ForeignKey(MicrorganismoGrupo, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Grupo')
    classe = models.ForeignKey(MicrorganismoClasse, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Classe')
    status = models.SmallIntegerField(choices=STATUS, default=1, verbose_name='Status')

    class Meta:
        ordering = ["microrganismo"]
        verbose_name = "Microrganismo"
        verbose_name_plural = "Microrganismos"

    def __str__(self):
        return self.microrganismo


class CepaRM(models.Model):
    __tablename__ = 'glass_cepa_rm'

    id = models.AutoField(primary_key=True, verbose_name='Código')
    cepa = models.CharField(max_length=50)
    status = models.SmallIntegerField(choices=STATUS, default=1, verbose_name='Status')

    class Meta:
        ordering = ["cepa"]
        verbose_name = "Cepa RM"
        verbose_name_plural = "Cepas :: RM"

    def __str__(self):
        return self.cepa


class CepaTSA(models.Model):
    __tablename__ = 'glass_cepa_tsa'

    id = models.AutoField(primary_key=True, verbose_name='Código')
    cepa = models.CharField(max_length=50)
    status = models.SmallIntegerField(choices=STATUS, default=1, verbose_name='Status')

    class Meta:
        ordering = ["cepa"]
        verbose_name = "Cepa TSA"
        verbose_name_plural = "Cepas :: TSA"

    def __str__(self):
        return self.cepa


class Gene(models.Model):
    __tablename__ = 'glass_gene'

    id = models.AutoField(primary_key=True, verbose_name='Código')
    gene = models.CharField(max_length=50)
    status = models.SmallIntegerField(choices=STATUS, default=1, verbose_name='Status')

    class Meta:
        ordering = ["gene"]
        verbose_name = "Gene de Resistência"
        verbose_name_plural = "Genes de Resistência"

    def __str__(self):
        return self.gene


class Material(models.Model):
    __tablename__ = 'glass_material'

    id = models.AutoField(primary_key=True, verbose_name='Código')
    material = models.CharField(max_length=80)
    status = models.SmallIntegerField(choices=STATUS, default=1, verbose_name='Status')

    class Meta:
        ordering = ["material"]
        verbose_name = "Material Biológico"
        verbose_name_plural = "Materiais Biológicos"

    def __str__(self):
        return self.material


class Metodo(models.Model):
    __tablename__ = 'glass_metodo'

    id = models.AutoField(primary_key=True, verbose_name='Código')
    metodo = models.CharField(max_length=50)
    status = models.SmallIntegerField(choices=STATUS, default=1, verbose_name='Status')

    class Meta:
        ordering = ["metodo"]
        verbose_name = "Método :: TSA"
        verbose_name_plural = "Métodos :: TSA"

    def __str__(self):
        return self.metodo


class NormaQualidade(models.Model):
    __tablename__ = 'glass_norma_qualidade'

    id = models.AutoField(primary_key=True, verbose_name='Código')
    norma = models.CharField(max_length=50)
    status = models.SmallIntegerField(choices=STATUS, default=1, verbose_name='Status')

    class Meta:
        ordering = ["norma"]
        verbose_name = "Norma :: Qualidade"
        verbose_name_plural = "Normas :: Qualidade"

    def __str__(self):
        return self.norma


class NormaAcreditacao(models.Model):
    __tablename__ = 'glass_norma_acreditacao'

    id = models.AutoField(primary_key=True, verbose_name='Código')
    norma = models.CharField(max_length=50)
    status = models.SmallIntegerField(choices=STATUS, default=1, verbose_name='Status')

    class Meta:
        ordering = ["norma"]
        verbose_name = "Norma :: Acreditação"
        verbose_name_plural = "Normas :: Acreditação"

    def __str__(self):
        return self.norma


class NormaTSA(models.Model):
    __tablename__ = 'glass_norma_tsa'

    id = models.AutoField(primary_key=True, verbose_name='Código')
    norma = models.CharField(max_length=50)
    status = models.SmallIntegerField(choices=STATUS, default=1, verbose_name='Status')

    class Meta:
        ordering = ["norma"]
        verbose_name = "Norma :: TSA"
        verbose_name_plural = "Normas :: TSA"

    def __str__(self):
        return self.norma


class Vinculo(models.Model):
    __tablename__ = 'glass_vinculo'

    id = models.AutoField(primary_key=True, verbose_name='Código')
    vinculo = models.CharField(max_length=50)
    status = models.SmallIntegerField(choices=STATUS, default=1, verbose_name='Status')

    class Meta:
        ordering = ["vinculo"]
        verbose_name = "Vínculo"
        verbose_name_plural = "Vínculos"

    def __str__(self):
        return self.vinculo


class Teste(models.Model):
    __tablename__ = 'glass_testes'

    id = models.AutoField(primary_key=True, verbose_name='Código')
    teste = models.CharField(max_length=50)
    status = models.SmallIntegerField(choices=STATUS, default=1, verbose_name='Status')

    class Meta:
        ordering = ["teste"]
        verbose_name = "Teste :: RM"
        verbose_name_plural = "Testes :: RM"

    def __str__(self):
        return self.teste


class Estabelecimento(models.Model):
    __tablename__ = 'glass_estabelecimento'
    # Estabelecimento
    id = models.AutoField(primary_key=True, verbose_name='Código')
    registro = models.CharField(max_length=5, choices=SISTEMAS, verbose_name='Registro Nacional')
    registro_id = models.CharField(max_length=12, verbose_name='Código Reg. Nac.')
    cnpj = models.CharField(max_length=20, verbose_name='CNPJ')
    razao_social = models.CharField(max_length=120, verbose_name='Nome ou Razão Social')
    municipio = models.CharField(max_length=120, verbose_name='Município')
    endereco = models.CharField(max_length=120, verbose_name='Endereço')
    telefone = models.CharField(max_length=120, verbose_name='Telefone')
    email = models.EmailField(max_length=254, verbose_name='E-mail')
    natureza = models.CharField(max_length=3, choices=NATUREZAS, verbose_name='Natureza da Instituição')
    esfera = models.CharField(max_length=3, choices=ESFERAS, verbose_name='Esfera Administrativa')
    vinculo = models.ForeignKey(Vinculo, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Vínculo Administrativo')
    servico = models.CharField(max_length=3, choices=SERVICOS, verbose_name='Prestação de serviço')
    lab_micro = models.CharField(max_length=3, choices=SIM_NAO, verbose_name='Lab. de Microbiologia')
    lab_apoio = models.CharField(max_length=120, verbose_name='Laboratório de Apoio')
    # Hospital
    leitos_total = models.IntegerField(verbose_name='Número de Leitos')
    leitos_uti = models.IntegerField(verbose_name='Número de Leitos UTI')
    leitos_uti_neo = models.IntegerField(verbose_name='Número de Leitos UTI-Neonatal')
    internacoes = models.IntegerField(verbose_name='Número  de Internações')
    atendimento = models.CharField(max_length=3, choices=ATENDIMENTOS, verbose_name='Tipos de Atendimento')
    nucleo_epidemio = models.CharField(max_length=3, choices=SIM_NAO, verbose_name='Núcleo Hospitalar de Epidemiologia')
    lic_sanitaria = models.CharField(max_length=3, choices=SIM_NAO, verbose_name='Licença Sanitária')
    sciras = models.CharField(max_length=3, choices=SIM_NAO, verbose_name='Serviço de Controle de Infecções Relacionadas à Assistência em Saúde')
    sinan = models.CharField(max_length=3, choices=NOT_SINAN, verbose_name='Notificação SINAN')
    corpo_clinico = models.CharField(max_length=3, choices=CLINICOS, verbose_name='Corpo Clínico')
    acreditacao = models.ManyToManyField(NormaAcreditacao, blank=True, verbose_name='Hospital Acreditado')
    # Laboratório
    gestao_qualidade = models.ManyToManyField(NormaQualidade, blank=True, verbose_name='Gestão da Qualidade')
    resp_tecnico = models.CharField(max_length=120, verbose_name='Resp. Técnico')
    prof_micro = models.CharField(max_length=3, choices=SIM_NAO, verbose_name='Prof. Microbiologista')
    interpretacao_tsa = models.ManyToManyField(NormaTSA, blank=True, verbose_name='Critério TSA')
    metodo_tsa = models.ManyToManyField(Metodo, blank=True, verbose_name='Método TSA')
    cq_tsa = models.CharField(max_length=3, choices=PERIODOS, verbose_name='Cont. Qualidade TSA')
    cepa_tsa = models.ManyToManyField(CepaTSA, blank=True, verbose_name='Cepas TSA')
    teste_rm = models.ManyToManyField(Teste, blank=True, verbose_name='Testes Fenotípicos')
    cq_rm = models.CharField(max_length=3, choices=PERIODOS, verbose_name='Cont. de Qualidade RM')
    cepa_rm = models.ManyToManyField(CepaRM, blank=True, verbose_name='Cepas para C.Q. RM')
    gene_rm = models.ManyToManyField(Gene, blank=True, verbose_name='Detecção RM')
    cultura = models.IntegerField(verbose_name='Quant. Culturas')
    hemocultura = models.IntegerField(verbose_name='Quant. Hemoculturas')
    urocultura = models.IntegerField(verbose_name='Quant. Uroculturas')
    neisseria = models.IntegerField(verbose_name='Quant. N. gonorrhoeae')
    classificacao = models.CharField(max_length=3, choices=CLASSIF, verbose_name='Classificação')
    data_cadastro = models.DateField(verbose_name='Data de Cadastro')
    data_atualizacao = models.DateField(verbose_name='Data de Atualização')
    status = models.SmallIntegerField(choices=STATUS, default=1, verbose_name='Status')

    class Meta:
        ordering = ["id"]
        verbose_name = "Estabelecimento"
        verbose_name_plural = "Estabelecimentos"

    def __str__(self):
        return self.razao_social

