# Generated by Django 2.0.2 on 2018-02-26 19:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('glass', '0009_auto_20180226_1912'),
    ]

    operations = [
        migrations.CreateModel(
            name='CepaRM',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='Código')),
                ('cepa', models.CharField(max_length=50)),
                ('status', models.SmallIntegerField(choices=[(1, 'Ativo'), (2, 'Inativo')], default=1, verbose_name='Status')),
            ],
            options={
                'verbose_name': 'Cepa RM',
                'verbose_name_plural': 'Cepas RM',
                'ordering': ['cepa'],
            },
        ),
        migrations.CreateModel(
            name='CepaTSA',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='Código')),
                ('cepa', models.CharField(max_length=50)),
                ('status', models.SmallIntegerField(choices=[(1, 'Ativo'), (2, 'Inativo')], default=1, verbose_name='Status')),
            ],
            options={
                'verbose_name': 'Cepa TSA',
                'verbose_name_plural': 'Cepas TSA',
                'ordering': ['cepa'],
            },
        ),
        migrations.AlterField(
            model_name='estabelecimento',
            name='classificacao',
            field=models.CharField(choices=[('US', 'Unidade Sentinela'), ('LA', 'Laboratório Apoio')], max_length=3, verbose_name='Classificação'),
        ),
    ]
