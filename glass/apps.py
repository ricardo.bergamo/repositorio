from django.apps import AppConfig


class GlassConfig(AppConfig):
    name = 'glass'
    label = "BR-GLASS"
    verbose_name = "BR-GLASS"
